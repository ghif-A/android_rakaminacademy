// data class to represent a player
data class Player(val name: String, var choice: Choice) {
    // override toString() method to print the player's name and choice
    override fun toString(): String {
        return "$name memilih ${choice.name}"
    }
}

// create an enum to represent the possible choices a player can make
enum class Choice(val beats: Choice?) {
    BATU(null),
    KERTAS(BATU),
    GUNTING(KERTAS);

    companion object {
        fun fromString(choiceString: String): Choice? {
            return when (choiceString.lowercase()) {
                "batu" -> BATU
                "kertas" -> KERTAS
                "gunting" -> GUNTING
                else -> null
            }
        }
    }
}

// create a class to represent the game
class Game(private val player1: Player, private val player2: Player) {
    // create a function to start the game
    fun play() {
        // print the players' choices
        println(player1)
        println(player2)

        // determine the winner
        val winner = when (player1.choice) {
            player2.choice -> null
            Choice.BATU -> if (player2.choice == Choice.GUNTING) player1 else player2
            Choice.KERTAS -> if (player2.choice == Choice.BATU) player1 else player2
            Choice.GUNTING -> if (player2.choice == Choice.KERTAS) player1 else player2
        }

        // print the result of the game
        if (winner == null) {
            println("Seri")
        } else {
            println("${winner.name} menang")
        }
    }
}

// main function
fun main() {
    // create two players
    println("GAME GUNTING BATU KERTAS")
    println("Player 1: ")
    val player1Choice = Choice.fromString(readln().lowercase())
    val player1 = Player("Player 1", player1Choice ?: Choice.GUNTING)
    println("Player 2: ")
    val player2Choice = Choice.fromString(readln().lowercase())
    val player2 = Player("Player 2", player2Choice ?: Choice.GUNTING)

    // create the game
    val game = Game(player1, player2)
    game.play()
}